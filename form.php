<!DOCTYPE html>
<head>
  <link rel="stylesheet" href="style.css">
  <meta charset="utf-8">
  <title> Глубокий космос</title>
</head>
<body >
  <header>
  <div class="banner">
  <a href="#home"><img id="logo" src="Pictures/1.png" width="100" height="100"  alt="Логотип"/></a>

  <h1>Быть может, ты найдешь ответы среди звезд</h1>
</div>
</header>

<div class="container">
  <div class="forma">
  <h2 id="form">Регистрация</h2>
  <form method="POST">
    <ol>
    <?php
        if (!empty($messages)) {
        print('<div id="messages">');
        foreach ($messages as $message) {
          print($message);
        }
        print('</div>');
        }
      ?>
      <li>ФИО: <input name="fio" placeholder="Введите ФИО" 
      <?php if ($errors['fio']) {print 'class="fioerror"';} ?>
       value="<?php print $values['fio']; ?>" /> 
      </li>
      <li>E-mail: <input name="email" placeholder="Введите email" type="email" 
      <?php if ($errors['email']) {print 'class="emailerror"';} ?> value="<?php print $values['email']; ?>" /></li>
      <li>Дата рождения/создания: <input name="yob" type="date"
      <?php if ($errors['yob']) {print 'class="yoberror"';} ?> value="<?php print $values['yob']; ?>" /> </li>
      <li>
        Пол:
        <input type="radio" checked="checked" name="gender" value="man"<?php if ($errors['gender']) {print 'class="gendererror"';} ?>/>Мужской
        <input type="radio" name="gender" value="woman"<?php if ($errors['gender']) {print 'class="gendererror"';} ?>/>Женский
        <input type="radio" name="gender" value="iskin"<?php if ($errors['gender']) {print 'class="gendererror"';} ?>/>Искин
        <input type="radio" name="gender" value="anomaly"<?php if ($errors['gender']) {print 'class="gendererror"';} ?>/>Аномалия
      </li>
      <li>
        Любимая цифра:
        <input type="radio" name="_value" <?php if ($errors['_value']) {print 'class="_valueerror"';} ?>value="1"/>1
        <input type="radio" name="_value" <?php if ($errors['_value']) {print 'class="_valueerror"';} ?>value="2"/>2
        <input type="radio" name="_value" <?php if ($errors['_value']) {print 'class="_valueerror"';} ?>value="3"/>3
        <input type="radio" checked="checked" name="_value" <?php if ($errors['_value']) {print 'class="_valueerror"';} ?> value="4"/>4
        <input type="radio" name="_value" <?php if ($errors['_value']) {print 'class="_valueerror"';} ?>value="5"/>5
        <input type="radio" name="_value" <?php if ($errors['_value']) {print 'class="_valueerror"';} ?>value="6"/>6
        <input type="radio" name="_value" <?php if ($errors['_value']) {print 'class="_valueerror"';} ?>value="7"/>7
        <input type="radio" name="_value" <?php if ($errors['_value']) {print 'class="_valueerror"';} ?>value="8"/>8
        <input type="radio" name="_value" <?php if ($errors['_value']) {print 'class="_valueerror"';} ?>value="9"/>9
      </li>
      <li>
        Предпочитаемый раздел науки:
        <select name="sp-sp" multiple="multiple" <?php if ($errors['sp-sp']) {print 'class="musicerror"';} ?>>
          <option value="rok">Физика</option>
          <option value="metall">Информатика</option>
          <option value="pop">Математика</option>
          <option value="wonder_sounds">Химия</option>
        </select>
      </li>
      <li>
        Биография :<textarea name="bio" 
        <?php if ($errors['bio']) {print 'class="bioerror"';} ?> 
        placeholder="Введите свою биографию: " ></textarea>
      </li>
      <li>
        <input type="checkbox" name="galka" <?php if ($errors['galka']) {print 'class="galkaerror"';} ?> />С условиями ознакомлен(а)
      </li>
      <li>
        <input type="submit" value="Отправить" />
      </li>
    </ol>
  </form>
</div>
</div>
<footer>
<h2>Реальность создают те же силы, которые ее и уничтожают</h2>
</footer>
</body>
</html>
